package main.java.com.trimindi.switching.client;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.util.SimpleLogListener;

import java.io.IOException;

/**
 * Created by sx on 12/05/17.
 */
public class ClientRequestListener implements ISORequestListener {
    private static org.jpos.util.Logger logger = new org.jpos.util.Logger();
    public ClientRequestListener() {
        super();
        logger.addListener(new SimpleLogListener(System.out));
    }

    @Override
    public boolean process(ISOSource isoSrc, ISOMsg isoMsg) {
        ISOMsg reply = (ISOMsg) isoMsg.clone();
        try {
            reply.setResponseMTI();
            reply.set(39, "00");
            reply.set(124, "response client nih..");
            isoSrc.send(reply);
        } catch (ISOException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
