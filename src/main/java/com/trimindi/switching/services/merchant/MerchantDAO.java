package main.java.com.trimindi.switching.services.merchant;

import main.java.com.trimindi.switching.base.BaseDAO;

/**
 * Created by HP on 20/05/2017.
 */
public class MerchantDAO extends BaseDAO implements MerchantDAOInterface<Merchant,String> {


    @Override
    public void persist(Merchant entity) {
        getCurrentSession().persist(entity);
    }

    @Override
    public void update(Merchant entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Merchant findById(String s) {
        Merchant merchant = (Merchant) getCurrentSession().get(Merchant.class,s);
        return merchant;
    }
}
