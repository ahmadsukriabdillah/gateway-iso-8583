package main.java.com.trimindi.switching.services.merchant;

/**
 * Created by HP on 20/05/2017.
 */
public class MerchantService {
    private MerchantDAO merchantDAO;
    public MerchantService(){
        this.merchantDAO = new MerchantDAO();
    }

    public boolean isAvailabel(String merchantid){
        this.merchantDAO.openCurrentSession();
        Merchant merchant = this.merchantDAO.findById(merchantid);
        this.merchantDAO.closeCurrentSession();
        return (merchant != null);
    }
}
