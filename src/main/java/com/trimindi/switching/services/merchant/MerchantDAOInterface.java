package main.java.com.trimindi.switching.services.merchant;

import java.io.Serializable;
import java.util.List;

/**
 * Created by HP on 20/05/2017.
 */
public interface MerchantDAOInterface <T, Id extends Serializable> {
    public void persist(T entity);
    public void update(T entity);
    public T findById(Id id);
}

