package main.java.com.trimindi.switching.services.partner.services;

import main.java.com.trimindi.switching.services.partner.dao.PartnerCredentialDAO;
import main.java.com.trimindi.switching.services.partner.models.PartnerCredential;

import java.util.List;

/**
 * Created by HP on 14/05/2017.
 */
public class PartnerCredentialService {
    PartnerCredentialDAO partnerCredentialDAO;

    public PartnerCredentialService() {
        partnerCredentialDAO = new PartnerCredentialDAO();
    }

    public PartnerCredential findByUserId(String id) {
        partnerCredentialDAO.openCurrentSession();
        PartnerCredential partnerCredential = partnerCredentialDAO.findByUserID(id);
        partnerCredentialDAO.closeCurrentSession();
        return partnerCredential;
    }

    public void update(PartnerCredential partnerCredential) {
        partnerCredentialDAO.openCurrentSessionwithTransaction();
        partnerCredentialDAO.update(partnerCredential);
        partnerCredentialDAO.closeCurrentSessionwithTransaction();
    }
    public List<PartnerCredential> findAll() {
        partnerCredentialDAO.openCurrentSession();
        List<PartnerCredential> partnerCredential = partnerCredentialDAO.findAll();
        partnerCredentialDAO.closeCurrentSession();
        return partnerCredential;
    }
}
