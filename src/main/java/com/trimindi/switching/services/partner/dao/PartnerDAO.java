package main.java.com.trimindi.switching.services.partner.dao;

import main.java.com.trimindi.switching.base.BaseDAO;
import main.java.com.trimindi.switching.services.partner.models.Partner;

import java.util.List;

/**
 * Created by HP on 14/05/2017.
 */
public class PartnerDAO extends BaseDAO implements PartnerDAOInterface<Partner,String> {
    @Override
    public void persist(Partner entity) {
        getCurrentSession().persist(entity);
    }

    @Override
    public void update(Partner entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Partner findById(String s) {
        return (Partner) getCurrentSession().get(Partner.class ,s);
    }

    @Override
    public void delete(Partner entity) {
    }

    @Override
    public List<Partner> findAll() {
        return null;
    }

    @Override
    public void deleteAll() {

    }
}
