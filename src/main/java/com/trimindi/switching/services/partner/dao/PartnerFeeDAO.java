package main.java.com.trimindi.switching.services.partner.dao;

import main.java.com.trimindi.switching.base.BaseDAO;
import main.java.com.trimindi.switching.services.partner.models.PartnerFee;

/**
 * Created by HP on 19/05/2017.
 */
public class PartnerFeeDAO extends BaseDAO {
    public PartnerFee findByPartnerID(String partnerid){
        return (PartnerFee) getCurrentSession().get(PartnerFee.class,partnerid);
    }
}
