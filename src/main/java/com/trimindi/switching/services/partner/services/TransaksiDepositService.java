package main.java.com.trimindi.switching.services.partner.services;

import main.java.com.trimindi.switching.services.partner.dao.TransDepositDAO;
import main.java.com.trimindi.switching.services.partner.models.TransaksiDeposit;

import java.util.List;

/**
 * Created by HP on 22/05/2017.
 */
public class TransaksiDepositService {
    private TransDepositDAO transDepositDAO;
    public TransaksiDepositService() {
        this.transDepositDAO = new TransDepositDAO();
    }

    public List<TransaksiDeposit> findAll(String partnerid){
        this.transDepositDAO.openCurrentSession();
        List<TransaksiDeposit> transaksiDeposits = this.transDepositDAO.findAll(partnerid);
        this.transDepositDAO.closeCurrentSession();
        return transaksiDeposits;
    }
}
