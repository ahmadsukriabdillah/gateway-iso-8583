package main.java.com.trimindi.switching.services.partner.dao;

import main.java.com.trimindi.switching.base.BaseDAO;
import main.java.com.trimindi.switching.services.partner.models.PartnerDeposit;

/**
 * Created by HP on 19/05/2017.
 */
public class PartnerDepositDAO extends BaseDAO {
    public void save(PartnerDeposit partnerDeposit){
        getCurrentSession().persist(partnerDeposit);
    }

    public void update(PartnerDeposit partnerDeposit){
        getCurrentSession().update(partnerDeposit);
    }
    public PartnerDeposit findByPartnerID(String partnerid){
        return (PartnerDeposit) getCurrentSession().get(PartnerDeposit.class,partnerid);
    }
}
