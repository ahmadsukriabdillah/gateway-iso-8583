package main.java.com.trimindi.switching.services.partner.services;

import main.java.com.trimindi.switching.services.partner.dao.PartnerFeeDAO;
import main.java.com.trimindi.switching.services.partner.models.PartnerFee;

/**
 * Created by HP on 19/05/2017.
 */
public class PartnerFeeService {
    private PartnerFeeDAO partnerFeeDAO;


    public PartnerFeeService(){
        partnerFeeDAO = new PartnerFeeDAO();
    }


    public PartnerFee findPartnerID(String partnerid){
        partnerFeeDAO.openCurrentSession();
        PartnerFee partnerFee = partnerFeeDAO.findByPartnerID(partnerid);
        partnerFeeDAO.closeCurrentSession();
        return partnerFee;
    }
}
