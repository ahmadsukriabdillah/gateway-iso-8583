package main.java.com.trimindi.switching.services.partner.services;

import main.java.com.trimindi.switching.services.partner.dao.PartnerDepositDAO;
import main.java.com.trimindi.switching.services.partner.dao.TransDepositDAO;
import main.java.com.trimindi.switching.services.partner.models.PartnerDeposit;
import main.java.com.trimindi.switching.services.partner.models.TransaksiDeposit;
import main.java.com.trimindi.switching.services.transaction.dao.TransactionDAO;
import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import main.java.com.trimindi.switching.utils.constanta.TStatus;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;

import java.sql.Timestamp;

/**
 * Created by HP on 19/05/2017.
 */
public class PartnerDepositService {
    private PartnerDepositDAO partnerDepositDAO;
    private TransactionDAO transactionDAO;
    private TransDepositDAO transDepositDAO;
    public PartnerDepositService(){
        this.partnerDepositDAO = new PartnerDepositDAO();
        this.transactionDAO = new TransactionDAO();
        this.transDepositDAO = new TransDepositDAO();
    }


    public boolean bookingSaldo(Transaction t){
        this.partnerDepositDAO.openCurrentSessionwithTransaction();
        PartnerDeposit p = partnerDepositDAO.findByPartnerID(t.getPARTNER_ID());
        if(p.getSaldo() < t.getTOTAL()){
            return false;
        }
        double saldo = p.getSaldo() - t.getTOTAL();
        p.setSaldo(saldo);
        p.setDebet(p.getDebet() + t.getTOTAL());
        t.setSALDO(saldo);
        t.setPAYDATE(new Timestamp(System.currentTimeMillis()));
        partnerDepositDAO.update(p);
        this.transDepositDAO.openCurrentSessionwithTransaction();
        TransaksiDeposit transaksiDeposit = new TransaksiDeposit();
        transaksiDeposit.setDebet(t.getTOTAL());
        transaksiDeposit.setSaldo(saldo);
        transaksiDeposit.setPartner_id(t.getPARTNER_ID());
        transaksiDeposit.setDescription("Payment " + t.getNTRANS());
        transaksiDeposit.setKredit(0);
        this.transDepositDAO.save(transaksiDeposit);
        this.transDepositDAO.closeCurrentSessionwithTransaction();
        this.transactionDAO.openCurrentSessionwithTransaction();
        transactionDAO.update(t);
        this.transactionDAO.closeCurrentSessionwithTransaction();
        this.partnerDepositDAO.closeCurrentSessionwithTransaction();
        return true;
    }


    public PartnerDeposit findPartnerDeposit(String partner_id) {
        this.partnerDepositDAO.openCurrentSession();
        PartnerDeposit partnerDeposit = partnerDepositDAO.findByPartnerID(partner_id);
        this.partnerDepositDAO.closeCurrentSession();
        return partnerDeposit;
    }

    public void reverse(Transaction t, ISOMsg isoMsg) throws ISOException {
        String payment = null;
        if(isoMsg != null){
            payment = new String(isoMsg.pack());
        }
        this.partnerDepositDAO.openCurrentSessionwithTransaction();
        PartnerDeposit p = partnerDepositDAO.findByPartnerID(t.getPARTNER_ID());
        double saldo = p.getSaldo() + t.getTOTAL();
        p.setSaldo(saldo);
        p.setDebet(p.getDebet() - t.getTOTAL());
        t.setREVERSE(payment);
        t.setKREDIT(t.getTOTAL());
        t.setREVERSEDATE(new Timestamp(System.currentTimeMillis()));
        partnerDepositDAO.update(p);
        this.partnerDepositDAO.closeCurrentSessionwithTransaction();
        this.transDepositDAO.openCurrentSessionwithTransaction();
        TransaksiDeposit transaksiDeposit = new TransaksiDeposit();
        transaksiDeposit.setDebet(0);
        transaksiDeposit.setSaldo(saldo);
        transaksiDeposit.setPartner_id(t.getPARTNER_ID());
        transaksiDeposit.setDescription("Reverse " + t.getNTRANS());
        transaksiDeposit.setKredit(t.getTOTAL());
        this.transDepositDAO.save(transaksiDeposit);
        this.transDepositDAO.closeCurrentSessionwithTransaction();
        this.transactionDAO.openCurrentSessionwithTransaction();
        t.setST(TStatus.REVERSE_PAYMENT_SUCCESS);
        transactionDAO.update(t);
        this.transactionDAO.closeCurrentSessionwithTransaction();
    }
}
