package main.java.com.trimindi.switching.services.partner.dao;

import main.java.com.trimindi.switching.base.BaseDAO;
import main.java.com.trimindi.switching.services.partner.models.PartnerCredential;
import org.hibernate.Query;

import java.util.List;

/**
 * Created by HP on 14/05/2017.
 */
public class PartnerCredentialDAO extends BaseDAO implements PartnerCredentialDAOInterface<PartnerCredential,String> {
    @Override
    public void persist(PartnerCredential entity) {

    }

    @Override
    public void update(PartnerCredential entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public PartnerCredential findById(String s) {
        return null;
    }

    @Override
    public PartnerCredential findByUserID(String userid) {
        List<PartnerCredential> p = getCurrentSession().getNamedQuery("findPartnerCredentialByUID")
                .setParameter("partner_uid",userid).list();
        return (p.size() == 1?p.get(0): null );
    }

    @Override
    public void delete(PartnerCredential entity) {

    }

    @Override
    public List<PartnerCredential> findAll() {
        List<PartnerCredential> p = getCurrentSession().getNamedQuery("findPartnerCredentialAll").list();
        return p;
    }

    @Override
    public void deleteAll() {

    }
}
