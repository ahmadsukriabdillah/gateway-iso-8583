package main.java.com.trimindi.switching.services.partner.dao;

import main.java.com.trimindi.switching.services.partner.models.TransaksiDeposit;

import java.io.Serializable;
import java.util.List;

/**
 * Created by HP on 22/05/2017.
 */
public interface TransDepositDAOInterface<T,Id extends Serializable> {
    void save(TransaksiDeposit transaksiDeposit);

    void update(TransaksiDeposit transaksiDeposit);

    List<T> findAll(String partnerid);
}
