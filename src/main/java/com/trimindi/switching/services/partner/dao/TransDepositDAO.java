package main.java.com.trimindi.switching.services.partner.dao;

import main.java.com.trimindi.switching.base.BaseDAO;
import main.java.com.trimindi.switching.services.partner.models.TransaksiDeposit;
import org.hibernate.Query;

import java.util.List;

/**
 * Created by HP on 22/05/2017.
 */
public class TransDepositDAO extends BaseDAO implements TransDepositDAOInterface<TransaksiDeposit,Integer> {

    @Override
    public void save(TransaksiDeposit transaksiDeposit) {
        getCurrentSession().persist(transaksiDeposit);
    }

    @Override
    public void update(TransaksiDeposit transaksiDeposit) {
        getCurrentSession().persist(transaksiDeposit);
    }

    @Override
    public List<TransaksiDeposit> findAll(String partnerid) {
        Query q = getCurrentSession().getNamedQuery("findTransaksiDepositByPartnerID").setString("partner_uid",partnerid);
        return (List<TransaksiDeposit>)q.list();
    }
}
