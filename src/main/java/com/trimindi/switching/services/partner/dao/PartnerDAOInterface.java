package main.java.com.trimindi.switching.services.partner.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by HP on 14/05/2017.
 */
public interface PartnerDAOInterface<T, Id extends Serializable> {
    public void persist(T entity);

    public void update(T entity);

    public T findById(Id id);

    public void delete(T entity);

    public List<T> findAll();

    public void deleteAll();
}
