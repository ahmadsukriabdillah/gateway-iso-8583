package main.java.com.trimindi.switching.services.partner.services;

import main.java.com.trimindi.switching.services.partner.dao.PartnerDAO;
import main.java.com.trimindi.switching.services.partner.models.Partner;

/**
 * Created by HP on 14/05/2017.
 */
public class PartnerService {
    private PartnerDAO partnerDAO;

    public PartnerService(){
        this.partnerDAO = new PartnerDAO();
    }

    public Partner findById(String partnerid){
        this.partnerDAO.openCurrentSession();
        Partner p = this.partnerDAO.findById(partnerid);
        this.partnerDAO.closeCurrentSession();
        return p;
    }
}
