package main.java.com.trimindi.switching.services.product_code.service;


import main.java.com.trimindi.switching.services.product_code.models.ProductCode;
import main.java.com.trimindi.switching.services.product_code.dao.ProductCodeDAO;

import java.util.List;

/**
 * Created by sx on 13/05/17.
 */
public class ProductCodeService {
    ProductCodeDAO productCodeDAO;

    public ProductCodeService() {
        this.productCodeDAO = new ProductCodeDAO();
    }


    public void persist(ProductCode entity) {
        productCodeDAO.openCurrentSessionwithTransaction();
        productCodeDAO.persist(entity);
        productCodeDAO.openCurrentSessionwithTransaction();
    }

    public void update(ProductCode entity) {
        productCodeDAO.openCurrentSessionwithTransaction();
        productCodeDAO.update(entity);
        productCodeDAO.openCurrentSessionwithTransaction();
    }

    public ProductCode findById(Long id) {
        productCodeDAO.openCurrentSessionwithTransaction();
        ProductCode book = productCodeDAO.findById(id);
        productCodeDAO.openCurrentSessionwithTransaction();
        return book;
    }
    public ProductCode findByDenom(String denom) {
        productCodeDAO.openCurrentSessionwithTransaction();
        ProductCode book = productCodeDAO.findByDenom(denom);
        productCodeDAO.openCurrentSessionwithTransaction();
        return book;
    }

    public void delete(Long id) {
        productCodeDAO.openCurrentSessionwithTransaction();
        ProductCode book = productCodeDAO.findById(id);
        productCodeDAO.delete(book);
        productCodeDAO.openCurrentSessionwithTransaction();
    }

    public List<ProductCode> findAll() {
        productCodeDAO.openCurrentSessionwithTransaction();
        List<ProductCode> books = productCodeDAO.findAll();
        productCodeDAO.openCurrentSessionwithTransaction();
        return books;
    }

    public void deleteAll() {
        productCodeDAO.openCurrentSessionwithTransaction();
        productCodeDAO.deleteAll();
        productCodeDAO.openCurrentSessionwithTransaction();
    }

    public ProductCodeDAO kodeProdukDAO() {
        return productCodeDAO;
    }
}
