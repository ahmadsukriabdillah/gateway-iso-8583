package main.java.com.trimindi.switching.services.product_code.dao;


import main.java.com.trimindi.switching.base.BaseDAO;
import main.java.com.trimindi.switching.services.product_code.models.ProductCode;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by sx on 12/05/17.
 */
public class ProductCodeDAO extends BaseDAO implements ProductCodeInterface<ProductCode, Long> {

    public ProductCodeDAO() {
    }

    @Override
    public void persist(ProductCode entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(ProductCode entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public ProductCode findById(Long aLong) {
        ProductCode productCode = (ProductCode) getCurrentSession().get(ProductCode.class, aLong);
        return productCode;
    }


    @Override
    public void delete(ProductCode entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<ProductCode> findAll() {
//        return getCurrentSession().createCriteria(ProductCode.class).list();
        List<ProductCode> p = getCurrentSession().getNamedQuery("findProductCodeAll").list();
        return p;
    }

    @Override
    public ProductCode findByDenom(String denom) {
        List<ProductCode> p = getCurrentSession().getNamedQuery("findProductCodeByDenom")
                .setParameter("denom",denom).list();
        return (p.size() >= 1?p.get(0): null);
    }

    @Override
    public void deleteAll() {
        List<ProductCode> productCodes = findAll();
        for (ProductCode entity : productCodes) {
            delete(entity);
        }
    }
}
