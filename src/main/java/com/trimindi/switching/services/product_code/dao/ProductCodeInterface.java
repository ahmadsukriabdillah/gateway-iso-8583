package main.java.com.trimindi.switching.services.product_code.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sx on 13/05/17.
 */
public interface ProductCodeInterface<T, Id extends Serializable> {
    public void persist(T entity);
    public void update(T entity);
    public T findById(Id id);
    public void delete(T entity);
    public List<T> findAll();
    public T findByDenom(String denom);
    public void deleteAll();
}
