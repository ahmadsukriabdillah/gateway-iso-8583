package main.java.com.trimindi.switching.services.transaction.service;

import main.java.com.trimindi.switching.packager.BukopinPackager;
import main.java.com.trimindi.switching.response.prepaid.InquiryResponse;
import main.java.com.trimindi.switching.services.partner.dao.PartnerFeeDAO;
import main.java.com.trimindi.switching.services.partner.models.PartnerCredential;
import main.java.com.trimindi.switching.services.partner.models.PartnerDeposit;
import main.java.com.trimindi.switching.services.partner.models.PartnerFee;
import main.java.com.trimindi.switching.services.product_code.dao.ProductCodeDAO;
import main.java.com.trimindi.switching.services.product_code.models.ProductCode;
import main.java.com.trimindi.switching.services.transaction.dao.TransactionDAO;
import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import main.java.com.trimindi.switching.utils.constanta.TStatus;
import main.java.com.trimindi.switching.utils.iso.models.Rules;
import main.java.com.trimindi.switching.utils.iso.parsing.SDE;
import main.java.com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPrePaid;
import org.jpos.iso.ISOMsg;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * Created by sx on 14/05/17.
 */
public class TransactionService {
    TransactionDAO transactionDAO;

    public TransactionService() {
        this.transactionDAO = new TransactionDAO();
    }


    public void persist(Transaction entity) {
        transactionDAO.openCurrentSessionwithTransaction();
        transactionDAO.persist(entity);
        transactionDAO.closeCurrentSessionwithTransaction();
    }

    public void update(Transaction entity) {
        transactionDAO.openCurrentSessionwithTransaction();
        transactionDAO.update(entity);
        transactionDAO.closeCurrentSessionwithTransaction();
    }

    public Transaction findById(String id) {
        transactionDAO.openCurrentSession();
        Transaction transaction = transactionDAO.findById(id);
        transactionDAO.closeCurrentSession();
        return transaction;
    }

    public Transaction findByMSSIDN(String mssidn){
        transactionDAO.openCurrentSession();
        Transaction transaction = transactionDAO.findByMSSIDN(mssidn);
        transactionDAO.closeCurrentSession();
        return transaction;
    }


    public void delete(String id) {
        transactionDAO.openCurrentSessionwithTransaction();
        Transaction book = transactionDAO.findById(id);
        transactionDAO.delete(book);
        transactionDAO.openCurrentSessionwithTransaction();
    }

    public void updateToUnderProsess(Transaction transaction) {
        transactionDAO.openCurrentSessionwithTransaction();
        transaction.setST(TStatus.PAYMENT_PROSESS);
        transactionDAO.update(transaction);
        transactionDAO.closeCurrentSessionwithTransaction();
    }

    public List<Transaction> findByBulanDanTahun(String bulan, String tahun,String partnerid) {
        transactionDAO.openCurrentSessionwithTransaction();
        List<Transaction> transaction = transactionDAO.findByBulanDanTahun(bulan,tahun,partnerid);
        transactionDAO.openCurrentSessionwithTransaction();
        return transaction;
    }

}
