package main.java.com.trimindi.switching.services.transaction.dao;

import main.java.com.trimindi.switching.base.BaseDAO;
import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import org.hibernate.Criteria;
import org.hibernate.Query;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by sx on 14/05/17.
 */


public class TransactionDAO extends BaseDAO implements TransactionDAOInterface<Transaction,String>{

    @Override
    public void persist(Transaction entity) {
        getCurrentSession().persist(entity);
    }

    @Override
    public void update(Transaction entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Transaction findById(String s) {
        Transaction transactions = (Transaction) getCurrentSession().get(Transaction.class,s);
        return transactions;
    }

    @Override
    public void delete(Transaction entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Transaction> findAll() {
        List<Transaction> transactions = (List<Transaction>) getCurrentSession().createQuery("from Transaction").list();
        return transactions;
    }

    @Override
    public void deleteAll() {

    }

    public Transaction findByMSSIDN(String mssidn) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Timestamp timestampadd = new Timestamp(System.currentTimeMillis());
        timestampadd.setTime(timestamp.getTime() + 2 * 60 * 1000);
        Query q = getCurrentSession().getNamedQuery("transaction.findByMssidn")
                .setString("mssidn",mssidn)
                .setTimestamp("dateStart",timestamp)
                .setTimestamp("dateend",timestampadd);
        return (Transaction) q.uniqueResult();
    }

    public List<Transaction> findByBulanDanTahun(String bulan, String tahun,String userid) {
        Query q = getCurrentSession().getNamedQuery("transaction.findByBulanDanTahun")
                .setString("BULAN",bulan)
                .setString("USER_ID",userid)
                .setString("TAHUN",tahun);
        return (List<Transaction>)q.list();
//        Query q = getCurrentSession().createQuery("select t from Transaction t left join fetch t.transactionStatus where t.BULAN = :BULAN and t.TAHUN = :TAHUN and t.USER_ID = :USER_ID ORDER BY t.DATE ASC")
//                .setString("BULAN",bulan)
//                .setString("USER_ID",userid)
//                .setString("TAHUN",tahun);
//        return (List<Transaction>)q.list();
    }
}
