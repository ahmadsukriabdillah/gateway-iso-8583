package main.java.com.trimindi.switching.services.transaction.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sx on 14/05/17.
 */
public interface TransactionDAOInterface<T,Id extends Serializable> {
    public void persist(T entity);
    public void update(T entity);
    public T findById(Id id);
    public void delete(T entity);
    public List<T> findAll();
    public void deleteAll();
}
