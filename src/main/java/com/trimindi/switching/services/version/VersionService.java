package main.java.com.trimindi.switching.services.version;

/**
 * Created by HP on 21/05/2017.
 */
public class VersionService {
    private VersionDAO versionDAO;
    public VersionService() {
        this.versionDAO =  new VersionDAO();
    }

    public Version find(){
        this.versionDAO.openCurrentSession();
        int id = 1;
        Version v = this.versionDAO.findById(1);
        this.versionDAO.closeCurrentSession();
        return v;
    }
}
