package main.java.com.trimindi.switching.services.version;

import main.java.com.trimindi.switching.base.BaseDAO;

/**
 * Created by HP on 21/05/2017.
 */
public class VersionDAO extends BaseDAO implements VersionDAOInterface<Version,Integer> {
    public VersionDAO() {
    }

    @Override
    public Version findById(Integer integer) {
        return (Version) getCurrentSession().get(Version.class,integer);
    }
}
