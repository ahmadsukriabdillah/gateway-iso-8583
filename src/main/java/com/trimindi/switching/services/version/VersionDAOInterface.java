package main.java.com.trimindi.switching.services.version;

import java.io.Serializable;
import java.util.List;

/**
 * Created by HP on 21/05/2017.
 */
public interface VersionDAOInterface<T,Id extends Serializable> {
    public T findById(Id id);
}
