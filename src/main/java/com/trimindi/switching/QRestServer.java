package main.java.com.trimindi.switching;

import main.java.com.trimindi.switching.client.ChannelManager;
import main.java.com.trimindi.switching.filter.CORSFilter;
import main.java.com.trimindi.switching.filter.SecureEndpoint;
import main.java.com.trimindi.switching.packager.BukopinPackager;
import main.java.com.trimindi.switching.utils.generator.PrePaidGenerator;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.jdom.Element;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.q2.QBeanSupport;
import org.jpos.q2.QFactory;
import org.jpos.util.NameRegistrar;

import java.lang.reflect.Constructor;
import java.util.Iterator;

/**
 * Created by sx on 11/05/17.
 */
public class QRestServer extends QBeanSupport implements QRestServerMBean {
    private Server server;
    private int port;
    private ChannelManager manager;
    public QRestServer() {
        super();
        try {
            manager = (ChannelManager) NameRegistrar.get("manager");

        } catch (NameRegistrar.NotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ISOMsg msg = manager.sendMsg(PrePaidGenerator.generateNetworkSignOn());
                    if (msg != null){
                        msg.setPackager(new BukopinPackager());
                        msg.dump(System.out,"Network SIGN ON Call");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    protected void startService() throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        ResourceConfig resourceConfig = resourcesConfig();
        resourceConfig.register(CORSFilter.class);
        resourceConfig.register(SecureEndpoint.class);
        context.addServlet(new ServletHolder(new ServletContainer(resourceConfig)), "/*");
        server = new Server(port);
//        server = new Server();
        server.setHandler(context);
//        ServerConnector connector = new ServerConnector(server);
//        connector.setPort(port);
//        HttpConfiguration https = new HttpConfiguration();
//        https.addCustomizer(new SecureRequestCustomizer());
//        SslContextFactory sslContextFactory = new SslContextFactory();
//        sslContextFactory.setKeyStorePath("ppob_switch.jks");
//        sslContextFactory.setKeyStorePassword("trimindimedia");
////        sslContextFactory.setKeyManagerPassword("trimindimedia");
//        ServerConnector sslConnector = new ServerConnector(server,
//                new SslConnectionFactory(sslContextFactory, "http/1.1"),
//                new HttpConnectionFactory(https));
//        sslConnector.setPort(9998);
//        server.setConnectors(new Connector[] { connector, sslConnector });
        server.start();
        NameRegistrar.register(getName(), this);
    }

    @SuppressWarnings("rawtypes")
    private ResourceConfig resourcesConfig() throws Exception {
        ResourceConfig resourceConfig = new ResourceConfig();

        QFactory factory = getFactory();
        Iterator iterator = getPersist().getChildren("rest-listener").iterator();
        while (iterator.hasNext()) {
            Element l = (Element) iterator.next();

            Class<?> cl = Class.forName(l.getAttributeValue ("class"));
            Constructor<?> cons = cl.getConstructor(ISOPackager.class);

            String packagerName = l.getAttributeValue ("packager");
            ISOPackager packager = null;
            if (packagerName != null) {
                packager = (ISOPackager) factory.newInstance (packagerName);
            }
            RestListener listener = (RestListener) cons.newInstance(packager);

            factory.setLogger (listener, l);
            factory.setConfiguration (listener, l);

            resourceConfig.register(listener);
        }
        return resourceConfig;
    }

    @Override
    protected void stopService() throws Exception {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ISOMsg msg = manager.sendMsg(PrePaidGenerator.generateNetworkSignOFF());
                    if (msg != null){
                        msg.setPackager(new BukopinPackager());
                        msg.dump(System.out,"Network SIGN OFF Call");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        server.stop();
        server.destroy();
        NameRegistrar.unregister(getName());
    }

    @Override
    public int getPort() {
        return this.port;
    }

    @Override
    public void setPort(int port) {
        this.port = port;
    }
}
