package main.java.com.trimindi.switching.response.prepaid;

import com.fasterxml.jackson.annotation.JsonIgnore;
import main.java.com.trimindi.switching.utils.generator.BaseHelper;
import main.java.com.trimindi.switching.utils.iso.models.Rules;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 * Created by HP on 17/05/2017.
 */

@XmlRootElement
public class InquiryResponse extends BaseHelper {
    private boolean status;
    private String SwitcherID;
    private String MeterSerialNumber;
    private String SubscriberID;
    private String Flag;
    private String PLNReferenceNumber;
    private String BukopinReferenceNumber;
    private String SubscriberName;
    private String SubscriberSegmentation;
    private int PowerConsumingCategory;
    private int MinorUnitofAdminCharge;
    private double AdminCharge;
    private String DistributionCode;
    private int ServiceUnit;
    private int ServiceUnitPhone;
    private int MaxKWHLimit;
    private int TotalRepeat;
    private double fee;
    private String ntrans;
    private double saldo;
    private double harga;

    public double getHarga() {
        return harga;
    }

    public InquiryResponse setHarga(double harga) {
        this.harga = harga;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public InquiryResponse setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    public String getNtrans() {
        return ntrans;
    }

    public InquiryResponse setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public double getFee() {
        return fee;
    }

    public InquiryResponse setFee(double fee) {
        this.fee = fee;
        return this;
    }

    public InquiryResponse(List<Rules> rules, boolean status) {
        this.status = status;
        this.SwitcherID = (String) rules.get(0).getResults();
        this.MeterSerialNumber = (String) rules.get(1).getResults();
        this.SubscriberID  = (String) rules.get(2).getResults();
        this.Flag  = (String) rules.get(3).getResults();
        this.PLNReferenceNumber  = (String) rules.get(4).getResults();
        this.BukopinReferenceNumber  = (String) rules.get(5).getResults();
        this.SubscriberName  = (String) rules.get(6).getResults();
        this.SubscriberSegmentation  = (String) rules.get(7).getResults();
        this.PowerConsumingCategory  = Integer.parseInt((String) rules.get(8).getResults());
        this.MinorUnitofAdminCharge  = Integer.parseInt((String) rules.get(9).getResults());
        this.AdminCharge  =numberMinorUnit ((String) rules.get(10).getResults(),this.MinorUnitofAdminCharge);
        this.DistributionCode = (String) rules.get(11).getResults();
        this.ServiceUnit = Integer.parseInt((String) rules.get(12).getResults());
        this.ServiceUnitPhone = Integer.parseInt((String) rules.get(13).getResults());
        this.MaxKWHLimit = Integer.parseInt((String) rules.get(14).getResults());
        this.TotalRepeat = Integer.parseInt((String) rules.get(15).getResults());

    }
    public InquiryResponse() {
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @XmlTransient @JsonIgnore
    public String getSwitcherID() {
        return SwitcherID;
    }

    public void setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
    }

    public String getMeterSerialNumber() {
        return MeterSerialNumber;
    }

    public void setMeterSerialNumber(String meterSerialNumber) {
        MeterSerialNumber = meterSerialNumber;
    }

    public String getSubscriberID() {
        return SubscriberID;
    }

    public void setSubscriberID(String subscriberID) {
        SubscriberID = subscriberID;
    }

    @XmlTransient @JsonIgnore
    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public String getPLNReferenceNumber() {
        return PLNReferenceNumber;
    }

    public void setPLNReferenceNumber(String PLNReferenceNumber) {
        this.PLNReferenceNumber = PLNReferenceNumber;
    }

    @XmlTransient @JsonIgnore
    public String getBukopinReferenceNumber() {
        return BukopinReferenceNumber;
    }

    public void setBukopinReferenceNumber(String bukopinReferenceNumber) {
        BukopinReferenceNumber = bukopinReferenceNumber;
    }

    public String getSubscriberName() {
        return SubscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        SubscriberName = subscriberName;
    }

    public String getSubscriberSegmentation() {
        return SubscriberSegmentation;
    }

    public void setSubscriberSegmentation(String subscriberSegmentation) {
        SubscriberSegmentation = subscriberSegmentation;
    }

    public int getPowerConsumingCategory() {
        return PowerConsumingCategory;
    }

    public void setPowerConsumingCategory(int powerConsumingCategory) {
        PowerConsumingCategory = powerConsumingCategory;
    }

    @XmlTransient @JsonIgnore
    public int getMinorUnitofAdminCharge() {
        return MinorUnitofAdminCharge;
    }

    public void setMinorUnitofAdminCharge(int minorUnitofAdminCharge) {
        MinorUnitofAdminCharge = minorUnitofAdminCharge;
    }

    public double getAdminCharge() {
        return AdminCharge;
    }

    public void setAdminCharge(double adminCharge) {
        AdminCharge = adminCharge;
    }

    @XmlTransient @JsonIgnore
    public String getDistributionCode() {
        return DistributionCode;
    }

    public void setDistributionCode(String distributionCode) {
        DistributionCode = distributionCode;
    }

    @XmlTransient @JsonIgnore
    public int getServiceUnit() {
        return ServiceUnit;
    }

    public void setServiceUnit(int serviceUnit) {
        ServiceUnit = serviceUnit;
    }

    @XmlTransient @JsonIgnore
    public int getServiceUnitPhone() {
        return ServiceUnitPhone;
    }

    public void setServiceUnitPhone(int serviceUnitPhone) {
        ServiceUnitPhone = serviceUnitPhone;
    }

    @XmlTransient @JsonIgnore
    public int getMaxKWHLimit() {
        return MaxKWHLimit;
    }

    public void setMaxKWHLimit(int maxKWHLimit) {
        MaxKWHLimit = maxKWHLimit;
    }

    @XmlTransient @JsonIgnore
    public int getTotalRepeat() {
        return TotalRepeat;
    }

    public void setTotalRepeat(int totalRepeat) {
        TotalRepeat = totalRepeat;
    }
}
