package main.java.com.trimindi.switching.response.postpaid;

import main.java.com.trimindi.switching.utils.iso.models.Rules;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by HP on 17/05/2017.
 */
@XmlRootElement
public class PaymentResponse {
    private boolean status;
    private String SwitcherID;
    private String SubscriberID;
    private int BillStatus;
    private int PaymentStatus;
    private int TotalOutstandingBill;
    private String BukopinReferenceNumber;
    private String SubscriberName;
    private int SubscriberUnit;
    private int ServiceUnitPhone;
    private String SubscriberSegmentation;
    private int PowerConsumingCategory;
    private double TotalAdminChargers;
    private String BillPeriod;
    private String DueDate;
    private String MeterReadDate;
    private double TotalElectricityBill;
    private String Incentive;
    private double ValueAddedTax;
    private double PenaltyFee;
    private String PreviousMeterReading1;
    private String CurrentMeterReading1;
    private String PreviousMeterReading2;
    private String CurrentMeterReading2;
    private String PreviousMeterReading3;
    private String CurrentMeterReading3;
    private String message;
    private double fee;
    private double harga;
    private String ntrans;
    private double saldo;

    public PaymentResponse() {
    }

    public PaymentResponse(List<Rules> rules) {
        this.status = true;
        this.SwitcherID = (String) rules.get(0).getResults();
        this.SubscriberID = (String) rules.get(1).getResults();
        this.BillStatus = Integer.parseInt((String) rules.get(2).getResults());
        this.PaymentStatus = Integer.parseInt((String) rules.get(3).getResults());
        this.TotalOutstandingBill = Integer.parseInt((String) rules.get(4).getResults());
        this.BukopinReferenceNumber = (String) rules.get(5).getResults();
        this.SubscriberName = (String) rules.get(6).getResults();
        this.SubscriberUnit = Integer.parseInt((String) rules.get(7).getResults());
        this.ServiceUnitPhone = Integer.parseInt((String) rules.get(8).getResults());
        this.SubscriberSegmentation = (String) rules.get(9).getResults();
        this.PowerConsumingCategory = Integer.parseInt((String) rules.get(10).getResults());
        this.TotalAdminChargers = Double.parseDouble((String) rules.get(11).getResults());
        this.BillPeriod = (String) rules.get(12).getResults();
        this.DueDate = (String) rules.get(13).getResults();
        this.MeterReadDate = (String) rules.get(14).getResults();
        this.TotalElectricityBill = Double.parseDouble((String) rules.get(15).getResults());
        this.Incentive = (String) rules.get(16).getResults();
        this.ValueAddedTax = Double.parseDouble((String) rules.get(17).getResults());
        this.PenaltyFee = Double.parseDouble((String) rules.get(18).getResults());
        this.PreviousMeterReading1 = (String) rules.get(19).getResults();
        this.CurrentMeterReading1 = (String) rules.get(20).getResults();
        this.PreviousMeterReading2 = (String) rules.get(21).getResults();
        this.CurrentMeterReading2 = (String) rules.get(22).getResults();
        this.PreviousMeterReading3 = (String) rules.get(23).getResults();
        this.CurrentMeterReading3 = (String) rules.get(24).getResults();
        this.message = (String) rules.get(25).getResults();
    }

    public boolean isStatus() {
        return status;
    }

    public PaymentResponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public String getSwitcherID() {
        return SwitcherID;
    }

    public PaymentResponse setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
        return this;
    }

    public String getSubscriberID() {
        return SubscriberID;
    }

    public PaymentResponse setSubscriberID(String subscriberID) {
        SubscriberID = subscriberID;
        return this;
    }

    public int getBillStatus() {
        return BillStatus;
    }

    public PaymentResponse setBillStatus(int billStatus) {
        BillStatus = billStatus;
        return this;
    }

    public int getPaymentStatus() {
        return PaymentStatus;
    }

    public PaymentResponse setPaymentStatus(int paymentStatus) {
        PaymentStatus = paymentStatus;
        return this;
    }

    public int getTotalOutstandingBill() {
        return TotalOutstandingBill;
    }

    public PaymentResponse setTotalOutstandingBill(int totalOutstandingBill) {
        TotalOutstandingBill = totalOutstandingBill;
        return this;
    }

    public String getBukopinReferenceNumber() {
        return BukopinReferenceNumber;
    }

    public PaymentResponse setBukopinReferenceNumber(String bukopinReferenceNumber) {
        BukopinReferenceNumber = bukopinReferenceNumber;
        return this;
    }

    public String getSubscriberName() {
        return SubscriberName;
    }

    public PaymentResponse setSubscriberName(String subscriberName) {
        SubscriberName = subscriberName;
        return this;
    }

    public int getSubscriberUnit() {
        return SubscriberUnit;
    }

    public PaymentResponse setSubscriberUnit(int subscriberUnit) {
        SubscriberUnit = subscriberUnit;
        return this;
    }

    public int getServiceUnitPhone() {
        return ServiceUnitPhone;
    }

    public PaymentResponse setServiceUnitPhone(int serviceUnitPhone) {
        ServiceUnitPhone = serviceUnitPhone;
        return this;
    }

    public String getSubscriberSegmentation() {
        return SubscriberSegmentation;
    }

    public PaymentResponse setSubscriberSegmentation(String subscriberSegmentation) {
        SubscriberSegmentation = subscriberSegmentation;
        return this;
    }

    public int getPowerConsumingCategory() {
        return PowerConsumingCategory;
    }

    public PaymentResponse setPowerConsumingCategory(int powerConsumingCategory) {
        PowerConsumingCategory = powerConsumingCategory;
        return this;
    }

    public double getTotalAdminChargers() {
        return TotalAdminChargers;
    }

    public PaymentResponse setTotalAdminChargers(double totalAdminChargers) {
        TotalAdminChargers = totalAdminChargers;
        return this;
    }

    public String getBillPeriod() {
        return BillPeriod;
    }

    public PaymentResponse setBillPeriod(String billPeriod) {
        BillPeriod = billPeriod;
        return this;
    }

    public String getDueDate() {
        return DueDate;
    }

    public PaymentResponse setDueDate(String dueDate) {
        DueDate = dueDate;
        return this;
    }

    public String getMeterReadDate() {
        return MeterReadDate;
    }

    public PaymentResponse setMeterReadDate(String meterReadDate) {
        MeterReadDate = meterReadDate;
        return this;
    }

    public double getTotalElectricityBill() {
        return TotalElectricityBill;
    }

    public PaymentResponse setTotalElectricityBill(double totalElectricityBill) {
        TotalElectricityBill = totalElectricityBill;
        return this;
    }

    public String getIncentive() {
        return Incentive;
    }

    public PaymentResponse setIncentive(String incentive) {
        Incentive = incentive;
        return this;
    }

    public double getValueAddedTax() {
        return ValueAddedTax;
    }

    public PaymentResponse setValueAddedTax(double valueAddedTax) {
        ValueAddedTax = valueAddedTax;
        return this;
    }

    public double getPenaltyFee() {
        return PenaltyFee;
    }

    public PaymentResponse setPenaltyFee(double penaltyFee) {
        PenaltyFee = penaltyFee;
        return this;
    }

    public String getPreviousMeterReading1() {
        return PreviousMeterReading1;
    }

    public PaymentResponse setPreviousMeterReading1(String previousMeterReading1) {
        PreviousMeterReading1 = previousMeterReading1;
        return this;
    }

    public String getCurrentMeterReading1() {
        return CurrentMeterReading1;
    }

    public PaymentResponse setCurrentMeterReading1(String currentMeterReading1) {
        CurrentMeterReading1 = currentMeterReading1;
        return this;
    }

    public String getPreviousMeterReading2() {
        return PreviousMeterReading2;
    }

    public PaymentResponse setPreviousMeterReading2(String previousMeterReading2) {
        PreviousMeterReading2 = previousMeterReading2;
        return this;
    }

    public String getCurrentMeterReading2() {
        return CurrentMeterReading2;
    }

    public PaymentResponse setCurrentMeterReading2(String currentMeterReading2) {
        CurrentMeterReading2 = currentMeterReading2;
        return this;
    }

    public String getPreviousMeterReading3() {
        return PreviousMeterReading3;
    }

    public PaymentResponse setPreviousMeterReading3(String previousMeterReading3) {
        PreviousMeterReading3 = previousMeterReading3;
        return this;
    }

    public String getCurrentMeterReading3() {
        return CurrentMeterReading3;
    }

    public PaymentResponse setCurrentMeterReading3(String currentMeterReading3) {
        CurrentMeterReading3 = currentMeterReading3;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public PaymentResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public double getFee() {
        return fee;
    }

    public PaymentResponse setFee(double fee) {
        this.fee = fee;
        return this;
    }

    public double getHarga() {
        return harga;
    }

    public PaymentResponse setHarga(double harga) {
        this.harga = harga;
        return this;
    }

    public String getNtrans() {
        return ntrans;
    }

    public PaymentResponse setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public PaymentResponse setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }
}
