package main.java.com.trimindi.switching.response.postpaid;

import com.fasterxml.jackson.annotation.JsonIgnore;
import main.java.com.trimindi.switching.utils.generator.BaseHelper;
import main.java.com.trimindi.switching.utils.iso.models.Rules;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;

/**
 * Created by HP on 18/05/2017.
 */

@XmlRootElement
public class InquiryResponse extends BaseHelper {
    private boolean status;
    private String SwitcherID;
    private String SubscriberID;
    private int BillStatus;
    private int TotalOutstandingBill;
    private String BukopinTbkReferenceNumber;
    private String SubscriberName;
    private String SubscriberUnit;
    private String ServiceUnitPhone;
    private String SubscriberSegmentation;
    private int PowerConsumingCategory;
    private double TotaladminChargers;
    private String BillPeriodrepeated;
    private String DueDaterepeated;
    private String MeterReadDaterepeated;
    private double TotalElectricityBillrepeated;
    private String Incentiverepeated;
    private double AddedTaxrepeated;
    private double PenaltyFeerepeated;
    private String PreviousMeterReading1;
    private double CurrentMeterReading1;
    private String PreviousMeterReading2repeated;
    private String CurrentMeterReading2repeated;
    private String PreviousMeterReading3repeated;
    private String CurrentMeterReading3repeated;
    private double fee;
    private String ntrans;
    private double saldo;
    private double harga;

    public InquiryResponse(List<Rules> rules, boolean status) {
        this.status = status;
        this.SwitcherID = (String) rules.get(0).getResults();
        this.SubscriberID = (String) rules.get(1).getResults();
        this.BillStatus = Integer.parseInt((String) rules.get(2).getResults());
        this.TotalOutstandingBill = Integer.parseInt((String) rules.get(3).getResults());
        this.BukopinTbkReferenceNumber = (String) rules.get(4).getResults();
        this.SubscriberName = (String) rules.get(5).getResults();
        this.SubscriberUnit = (String) rules.get(6).getResults();
        this.ServiceUnitPhone = (String) rules.get(7).getResults();
        this.SubscriberSegmentation = (String) rules.get(8).getResults();
        this.PowerConsumingCategory = Integer.parseInt((String) rules.get(9).getResults());
        this.TotaladminChargers = Double.parseDouble((String) rules.get(10).getResults());
        this.BillPeriodrepeated = (String) rules.get(11).getResults();
        this.DueDaterepeated = (String) rules.get(12).getResults();
        this.MeterReadDaterepeated = (String) rules.get(13).getResults();
        this.TotalElectricityBillrepeated = Double.parseDouble((String) rules.get(14).getResults());
        this.Incentiverepeated = (String) rules.get(15).getResults();
        this.AddedTaxrepeated = Double.parseDouble((String) rules.get(16).getResults());
        this.PenaltyFeerepeated = Double.parseDouble((String) rules.get(17).getResults());
        this.PreviousMeterReading1 = (String) rules.get(18).getResults();
        this.CurrentMeterReading1 = Double.parseDouble((String) rules.get(19).getResults());
        this.PreviousMeterReading2repeated = (String) rules.get(20).getResults();
        this.CurrentMeterReading2repeated = (String) rules.get(21).getResults();
        this.PreviousMeterReading3repeated = (String) rules.get(22).getResults();
        this.CurrentMeterReading3repeated = (String) rules.get(23).getResults();

    }

    public InquiryResponse() {
    }

    public String getNtrans() {
        return ntrans;
    }

    public InquiryResponse setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public double getFee() {
        return fee;
    }

    public InquiryResponse setFee(double fee) {
        this.fee = fee;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public InquiryResponse setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    public double getHarga() {
        return harga;
    }

    public InquiryResponse setHarga(double harga) {
        this.harga = harga;
        return this;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @XmlTransient @JsonIgnore
    public String getSwitcherID() {
        return SwitcherID;
    }

    public void setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
    }

    public String getSubscriberID() {
        return SubscriberID;
    }

    public void setSubscriberID(String subscriberID) {
        SubscriberID = subscriberID;
    }

    public int getBillStatus() {
        return BillStatus;
    }

    public void setBillStatus(int billStatus) {
        BillStatus = billStatus;
    }

    public int getTotalOutstandingBill() {
        return TotalOutstandingBill;
    }

    public void setTotalOutstandingBill(int totalOutstandingBill) {
        TotalOutstandingBill = totalOutstandingBill;
    }

    @XmlTransient @JsonIgnore
    public String getBukopinTbkReferenceNumber() {
        return BukopinTbkReferenceNumber;
    }

    public void setBukopinTbkReferenceNumber(String bukopinTbkReferenceNumber) {
        BukopinTbkReferenceNumber = bukopinTbkReferenceNumber;
    }

    public String getSubscriberName() {
        return SubscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        SubscriberName = subscriberName;
    }

    @XmlTransient @JsonIgnore
    public String getSubscriberUnit() {
        return SubscriberUnit;
    }

    public void setSubscriberUnit(String subscriberUnit) {
        SubscriberUnit = subscriberUnit;
    }

    @XmlTransient @JsonIgnore
    public String getServiceUnitPhone() {
        return ServiceUnitPhone;
    }

    public void setServiceUnitPhone(String serviceUnitPhone) {
        ServiceUnitPhone = serviceUnitPhone;
    }

    public String getSubscriberSegmentation() {
        return SubscriberSegmentation;
    }

    public void setSubscriberSegmentation(String subscriberSegmentation) {
        SubscriberSegmentation = subscriberSegmentation;
    }

    public int getPowerConsumingCategory() {
        return PowerConsumingCategory;
    }

    public void setPowerConsumingCategory(int powerConsumingCategory) {
        PowerConsumingCategory = powerConsumingCategory;
    }

    public double getTotaladminChargers() {
        return TotaladminChargers;
    }

    public void setTotaladminChargers(double totaladminChargers) {
        TotaladminChargers = totaladminChargers;
    }

    public String getBillPeriodrepeated() {
        return BillPeriodrepeated;
    }

    public void setBillPeriodrepeated(String billPeriodrepeated) {
        BillPeriodrepeated = billPeriodrepeated;
    }

    public String getDueDaterepeated() {
        return DueDaterepeated;
    }

    public void setDueDaterepeated(String dueDaterepeated) {
        DueDaterepeated = dueDaterepeated;
    }

    public String getMeterReadDaterepeated() {
        return MeterReadDaterepeated;
    }

    public void setMeterReadDaterepeated(String meterReadDaterepeated) {
        MeterReadDaterepeated = meterReadDaterepeated;
    }

    public double getTotalElectricityBillrepeated() {
        return TotalElectricityBillrepeated;
    }

    public void setTotalElectricityBillrepeated(double totalElectricityBillrepeated) {
        TotalElectricityBillrepeated = totalElectricityBillrepeated;
    }

    @XmlTransient @JsonIgnore
    public String getIncentiverepeated() {
        return Incentiverepeated;
    }

    public void setIncentiverepeated(String incentiverepeated) {
        Incentiverepeated = incentiverepeated;
    }

    public double getAddedTaxrepeated() {
        return AddedTaxrepeated;
    }

    public void setAddedTaxrepeated(double addedTaxrepeated) {
        AddedTaxrepeated = addedTaxrepeated;
    }

    public double getPenaltyFeerepeated() {
        return PenaltyFeerepeated;
    }

    public void setPenaltyFeerepeated(double penaltyFeerepeated) {
        PenaltyFeerepeated = penaltyFeerepeated;
    }

    public String getPreviousMeterReading1() {
        return PreviousMeterReading1;
    }

    public void setPreviousMeterReading1(String previousMeterReading1) {
        PreviousMeterReading1 = previousMeterReading1;
    }

    public double getCurrentMeterReading1() {
        return CurrentMeterReading1;
    }

    public void setCurrentMeterReading1(double currentMeterReading1) {
        CurrentMeterReading1 = currentMeterReading1;
    }

    @XmlTransient @JsonIgnore
    public String getPreviousMeterReading2repeated() {
        return PreviousMeterReading2repeated;
    }

    public void setPreviousMeterReading2repeated(String previousMeterReading2repeated) {
        PreviousMeterReading2repeated = previousMeterReading2repeated;
    }

    @XmlTransient @JsonIgnore
    public String getCurrentMeterReading2repeated() {
        return CurrentMeterReading2repeated;
    }

    public void setCurrentMeterReading2repeated(String currentMeterReading2repeated) {
        CurrentMeterReading2repeated = currentMeterReading2repeated;
    }
    @XmlTransient @JsonIgnore
    public String getPreviousMeterReading3repeated() {
        return PreviousMeterReading3repeated;
    }

    public void setPreviousMeterReading3repeated(String previousMeterReading3repeated) {
        PreviousMeterReading3repeated = previousMeterReading3repeated;
    }

    @XmlTransient @JsonIgnore
    public String getCurrentMeterReading3repeated() {
        return CurrentMeterReading3repeated;
    }

    public void setCurrentMeterReading3repeated(String currentMeterReading3repeated) {
        CurrentMeterReading3repeated = currentMeterReading3repeated;
    }
}
