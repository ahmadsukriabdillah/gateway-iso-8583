package main.java.com.trimindi.switching.response;

import main.java.com.trimindi.switching.services.transaction.models.Transaction;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by HP on 23/05/2017.
 */
@XmlRootElement
public class ReportResponse {
    private boolean status;
    private List<Transaction> transactionList;

    public boolean isStatus() {
        return status;
    }

    public ReportResponse() {
    }

    public ReportResponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public ReportResponse setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
        return this;
    }
}
