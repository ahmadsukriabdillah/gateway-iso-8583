package main.java.com.trimindi.switching.base;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Created by sx on 13/05/17.
 */
public class BaseDAO {
    private Session currentSession;

    private Transaction currentTransaction;

    public BaseDAO() {
    }

    public Session openCurrentSession() {
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionwithTransaction() {
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession() {
        currentSession.close();
    }
    public void closeCurrentSessionwithTransaction() {
        currentTransaction.commit();
        currentSession.close();
    }

    protected static SessionFactory getSessionFactory() {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        return sessionFactory;
    }

    protected Session getCurrentSession() {
        return currentSession;
    }

    protected void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    protected Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    protected void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }
}
