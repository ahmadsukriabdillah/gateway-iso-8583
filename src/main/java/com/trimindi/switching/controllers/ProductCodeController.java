package main.java.com.trimindi.switching.controllers;

import main.java.com.trimindi.switching.RestListener;
import main.java.com.trimindi.switching.client.ChannelManager;
import main.java.com.trimindi.switching.services.product_code.models.ProductCode;
import main.java.com.trimindi.switching.services.product_code.service.ProductCodeService;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOPackager;
import org.jpos.util.LogSource;
import org.jpos.util.NameRegistrar;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by sx on 11/05/17.
 */

@Path("/produk")
public class ProductCodeController extends RestListener implements LogSource, Configurable {
    private ProductCodeService service;
    private ChannelManager channelManager;
    public ProductCodeController(ISOPackager packager) {
        super(packager);
        try {
            service = new ProductCodeService();
            channelManager = (ChannelManager) NameRegistrar.get("manager");
        } catch (NameRegistrar.NotFoundException e) {
            e.printStackTrace();
        }
    }
    @GET
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<ProductCode> getAllProduct() {
        return service.findAll();
    }

    @POST
    @Path("/")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response createProduct(ProductCode produk) {
        service.persist(produk);
        return Response.status(200).build();
    }

    @PUT
    @Path("/{product_id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response updateProduct(ProductCode produk, @PathParam("product_id") Long s) {
        ProductCode u = service.findById(s);
        u.setProduct_item(produk.getProduct_item());
        u.setChannel(produk.isChannel());
        u.setPos(produk.isPos());
        service.persist(u);
        return Response.status(200).build();
    }

    @DELETE
    @Path("/{product_id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response deleteProduct(@PathParam("product_id") Long s) {
        service.delete(s);
        return Response.status(200).build();
    }

    @Override
    public void setConfiguration(Configuration configuration) throws ConfigurationException {

    }
}
