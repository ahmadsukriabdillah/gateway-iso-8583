package main.java.com.trimindi.switching.utils.constanta;

/**
 * Created by HP on 20/05/2017.
 */
public class Constanta {
    public static final String MAC = "MAC";
    public static final String LATITUDE = "LATITUDE";
    public static final String LONGITUDE = "LONGITUDE";
    public static final String USER_ID = "USER_ID";
    public static final String TIME = "TIME";
    public static final String SIGN = "SIGN";
    public static final String IP_ADDRESS = "IP ADDRESS";
    public static final String PRINCIPAL = "principal";
    public static final String DENOM = "DENOM";
    public static final String MACHINE_TYPE = "MT";
    public static final String MSSIDN = "MSSIDN";
    public static final String SUCCESS_PAYMENT = "MSSIDN";

    public static final String POSTPAID_PRODUCT_CODE = "9";
    public static final String NONTAGLIST_PROCODE = "99504";
    public static final String IS = "msr";
}
