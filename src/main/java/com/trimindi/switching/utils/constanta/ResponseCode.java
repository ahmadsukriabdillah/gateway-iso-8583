package main.java.com.trimindi.switching.utils.constanta;

import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by sx on 13/05/17.
 */
@XmlRootElement
public class ResponseCode {
    public static final ResponseCode SERVER_UNAVAILABLE = new ResponseCode("9999","Server Unavailable");
    public static final ResponseCode SERVER_TIMEOUT = new ResponseCode("9998", "Request Timeout");
    public static final ResponseCode PARAMETER_MERCHANT_TYPE = new ResponseCode("9997","Merchant type not registerd.");
    public static final ResponseCode PARAMETER_SALDO = new ResponseCode("1003","Saldo anda tidak mencukupi.");
    public static final ResponseCode PARAMETER_UNKNOW_DENOM = new ResponseCode("1002","Denom not recognize.");
    public static final ResponseCode NTRANS_NOT_FOUND = new ResponseCode("1006","Ntrans Not Found");
    public static final ResponseCode MISSING_REQUIRED_PARAMETER = new ResponseCode("9999","Missing Required paramater");
    public static final ResponseCode PAYMENT_UNDER_PROSES = new ResponseCode("1007","Payment under prosess please wait.");
    public static final ResponseCode PAYMENT_DONE = new ResponseCode("1008","Ntrans expired");
    public static final ResponseCode PAYMENT_FAILED = new ResponseCode("1009","Pembelian Gagal");
    public static final ResponseCode MAXIMUM_PRINTED_RECIPT = new ResponseCode("1010", "Exceed Maximum printed recipt");
    private boolean status;
    private String code;
    private String message;

    public ResponseCode() {
    }

    public boolean isStatus() {
        return status;
    }

    public ResponseCode setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public ResponseCode(String code, String message) {
        this.code = code;
        this.status = false;
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResponseCode{" +
                "status=" + status +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public String getcode() {
        return code;
    }

    public void setcode(String code) {
        this.code = code;
    }

    public String getmessage() {
        return message;
    }

    public void setmessage(String message) {
        this.message = message;
    }
}
