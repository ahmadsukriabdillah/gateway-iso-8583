package test;

import main.java.com.trimindi.switching.utils.iso.models.Rules;
import main.java.com.trimindi.switching.utils.iso.parsing.SDE;
import main.java.com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPostPaid;
import org.junit.Test;

import java.util.List;

/**
 * Created by HP on 18/05/2017.
 */
public class ParsingISOPostPaidTest {

    @Test
    public void parsingInquiry(){
        String payload = "0000000530000000001101040DE32293570C0390A0B71C2167DE35SUBCRIBER NAME           00005000000000000015  R10000013000000025002017051805201718052017000000300000C00000000000000000000000000000000000011110000222200000008000000080000000800000008";
        List<Rules> rules = new SDE.Builder().setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48,true)).setPayload(payload).generate();
        System.out.println(rules);
    }
}
