package test;

import main.java.com.trimindi.switching.services.transaction.models.Transaction;
import main.java.com.trimindi.switching.utils.generator.NonTagListGenerator;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.junit.Test;

/**
 * Created by HP on 15/05/2017.
 */
public class GeneratorISONonTagListTest {
    @Test
    public void generateNetworkSignOn() throws ISOException {
        ISOMsg msg = NonTagListGenerator.generateNetworkSignOn();
        msg.dump(System.out,"Network Sign-ON");
        System.out.println(new String(msg.pack()));
    }
    @Test
    public void generateNetworkSignOff() throws ISOException {
        ISOMsg msg = NonTagListGenerator.generateNetworkSignOFF();
        msg.dump(System.out,"Network Sign-OFF");
        System.out.println(new String(msg.pack()));

    }
    @Test
    public void generateNetworkEchoTest() throws ISOException {
        ISOMsg msg = NonTagListGenerator.generateNetworkEchoTest();
        msg.dump(System.out,"Network Echo Test");
        System.out.println(new String(msg.pack()));

    }


    @Test
    public void generateInqury() throws ISOException {
        ISOMsg msg = NonTagListGenerator.generateInquiry("5401","102030129301");
        msg.dump(System.out,"Inquiry");
        System.out.println(new String(msg.pack()));

    }

    @Test
    public void generatePayment() throws ISOException {
        ISOMsg msg = NonTagListGenerator.generatePurchase(new Transaction());
        msg.dump(System.out, "Payment");
        System.out.println(new String(msg.pack()));
    }

}
