package test;

import main.java.com.trimindi.switching.utils.iso.models.Rules;
import main.java.com.trimindi.switching.utils.iso.parsing.SDE;
import main.java.com.trimindi.switching.utils.rules.response.ResponseRulesGeneratorPrePaid;
import org.junit.Test;

import java.util.List;

/**
 * Created by HP on 17/05/2017.
 */
public class ParsingISOPrePaidTest {
    @Test
    public void parsingInquiry(){
        String payload = "0000000110000000865300000000010                                6162927BAB078C6FE5B349A3DE5021CBSUBSCRIBER NAME            R100000130020000250000";
        List<Rules> bit48 = new SDE.Builder().setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(48,true))
                .setPayload(payload)
                .generate();
        System.out.print(bit48.toString());
//        payload = "0200005000000000000015000050";
//        List<Rules> bit62 = new SDE.Builder().setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(62,true))
//                .setPayload(payload)
//                .generate();
//        System.out.print(bit62.toString());
    }

    @Test
    public void parsingPayment(){
        String payload = "00000001100000008653000000000105B726575210CF561024A4CD40E819E81044122C9B15A16A29F302D747B3372FA00000008SUBSCRIBER NAME          R1  0000013000200002500002000000000020000000000200000000002000000000020000017500002000000128012312312311231231231";
        List<Rules> bit48 = new SDE.Builder().setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(48,true)).setPayload(payload).generate();
//        System.out.print(bit48.toString());
        payload = "020500000000000001500005000000000000";
        List<Rules> bit62 = new SDE.Builder().setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(62,true)).setPayload(payload).generate();
        System.out.print(bit62);
    }
}
